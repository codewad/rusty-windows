#include <windows.h>
#include <windowsx.h>
#include <wingdi.h>
#include <stdio.h>

extern LRESULT window_proc(void *context, HWND window_handle, UINT message, WPARAM wparam, LPARAM lparam, int *handled);
extern BOOL native_wm_showwindow(HWND window_handle, BOOL shown, UINT param);
static void *context = NULL;

void make_context(void *_context)
{
    context = _context;
}

void set_mouse_position(HWND handle, int x, int y)
{
    POINT point;
    point.x = x;
    point.y = y;
    ClientToScreen(handle, &point);

    SetCursorPos(point.x, point.y);
}

int set_track_mouse_event(HWND handle)
{
    TRACKMOUSEEVENT mouse_event;
    mouse_event.cbSize = sizeof(TRACKMOUSEEVENT);
    mouse_event.dwFlags = TME_LEAVE;
    mouse_event.hwndTrack = handle;
    mouse_event.dwHoverTime = HOVER_DEFAULT;

    return TrackMouseEvent(&mouse_event);
}

int get_x_param(WPARAM wparam)
{
    return GET_X_LPARAM(wparam);
}

int get_y_param(WPARAM wparam)
{
    return GET_Y_LPARAM(wparam);
}

int get_wheel_delta_wparam(WPARAM wparam)
{
    return GET_WHEEL_DELTA_WPARAM(wparam);
}

static LRESULT CALLBACK WindowProc(HWND window_handle, UINT message, WPARAM wParam, LPARAM lParam)
{
    int handled = 0;
    LRESULT result = window_proc(context, window_handle, message, wParam, lParam, &handled);

    if (handled)
        return result;

    switch (message)
    {
    case WM_DESTROY:
        printf("WM_DESTROY\n");
        PostQuitMessage(0);
        return 0;
    }

    return DefWindowProc(window_handle, message, wParam, lParam);
}

HWND create_window()
{
    HINSTANCE instance = GetModuleHandle(NULL);
    const char *CLASS_NAME = "window_class";
    WNDCLASSEX windowClass;

    windowClass.cbSize        = sizeof(WNDCLASSEX);
    windowClass.style         = CS_VREDRAW | CS_HREDRAW;
    windowClass.lpfnWndProc   = WindowProc;
    windowClass.cbClsExtra    = 0;
    windowClass.cbWndExtra    = 0;
    windowClass.hInstance     = instance;
    windowClass.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
    windowClass.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);
    windowClass.hCursor       = LoadCursor(NULL, IDC_ARROW);
    windowClass.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);
    windowClass.lpszClassName = CLASS_NAME;
    windowClass.lpszMenuName  = NULL;

    RegisterClassEx(&windowClass);

    HWND handle = CreateWindowEx(0, CLASS_NAME, "The Window", WS_OVERLAPPEDWINDOW,
                          CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
                          NULL, NULL, instance, NULL);

    ShowWindow(handle, SW_SHOW);

    return handle;
}

void do_message_loop(HWND handle)
{
    MSG msg;
    int result;
    while ((result = GetMessage(&msg, handle, 0, 0)) != 0)
    {
        // TODO: There should be some better error handling here.
        if (result == -1) {
            printf("GetMessage error: ");
            char *message;
            FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                           NULL, GetLastError(),
                           MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                           (LPSTR)&message, 0, NULL);
            printf("%s", message);
            break;
        }
        DispatchMessage(&msg);
    }
}

void close_window(HWND handle)
{
    SendNotifyMessage(handle, WM_CLOSE, 0, 0);
}
