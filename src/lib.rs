use libc;

pub mod windows;
pub mod input_types;
pub mod opengl;

pub use windows::gl;
