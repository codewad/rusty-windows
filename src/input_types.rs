#[derive(Debug, Clone, Copy)]
pub enum MouseButton
{
    Left,
    Right,
    Middle
}

#[derive(Debug, Clone, Copy)]
pub enum Key
{
    Backspace,
    Tab,
    Enter,
    Shift,
    Ctrl,
    CapsLock,
    Escape,
    Space,
    PgUp,
    PgDown,
    End,
    Home,
    LeftArrow,
    RightArrow,
    UpArrow,
    DownArrow,
    Insert,
    Delete,
    K0, K1, K2, K3, K4, K5, K6, K7, K8, K9,
    KA, KB, KC, KD, KE, KF, KG, KH, KI, KJ,
    KK, KL, KM, KN, KO, KP, KQ, KR, KS, KT,
    KU, KV, KW, KX, KY, KZ,
}

#[derive(Debug, Clone, Copy)]
pub enum WindowMessage
{
    // Close,
    Keydown { key: Option<Key> },
    Keyup { key: Option<Key> },
    MouseMove { x: i32, y: i32 },
    MouseButtonDown { button: MouseButton },
    MouseButtonUp { button: MouseButton },
    MouseWheelScroll { delta: i32 }
}
