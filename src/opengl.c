#include <windows.h>
#include <stdio.h>

#define WGL_CONTEXT_MAJOR_VERSION_ARB 0x2091
#define WGL_CONTEXT_MINOR_VERSION_ARB 0x2092
#define WGL_CONTEXT_FLAGS_ARB 0x2094

static HMODULE module = NULL;

HGLRC create_opengl_context(HDC device_context)
{
    PIXELFORMATDESCRIPTOR pixel_format_descriptor;
    memset(&pixel_format_descriptor, 0, sizeof(PIXELFORMATDESCRIPTOR));
    pixel_format_descriptor.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pixel_format_descriptor.nVersion = 1;
    pixel_format_descriptor.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pixel_format_descriptor.iPixelType = PFD_TYPE_RGBA;
    pixel_format_descriptor.cColorBits = 32;
    pixel_format_descriptor.cDepthBits = 32;
    pixel_format_descriptor.iLayerType = PFD_MAIN_PLANE;

    int pixel_format_index = ChoosePixelFormat(device_context, &pixel_format_descriptor);
    SetPixelFormat(device_context, pixel_format_index, &pixel_format_descriptor);
    HGLRC temp_context = wglCreateContext(device_context);
    wglMakeCurrent(device_context, temp_context);

    int attributes[] = {
        WGL_CONTEXT_MAJOR_VERSION_ARB, 4,
        WGL_CONTEXT_MINOR_VERSION_ARB, 5,
        WGL_CONTEXT_FLAGS_ARB, 0,
        0
    };

    PROC wglCreateContextAttribsARB = wglGetProcAddress("wglCreateContextAttribsARB");
    HGLRC context = (HGLRC)wglCreateContextAttribsARB(device_context, 0, attributes);
    wglMakeCurrent(NULL, NULL);
    wglDeleteContext(temp_context);

    return context;
}

PROC oglLoadProc(const char *name)
{
  PROC result = wglGetProcAddress(name);
  if (result == NULL) {
    if (module == NULL) {
      module = LoadLibraryA("opengl32.dll");
    }
    result = GetProcAddress(module, name);
  }
  return result;
}
