extern crate libc;

use std::ffi::CString;
use std::sync::mpsc::Sender;
use std::collections::HashMap;
use crate::input_types::{MouseButton, Key, WindowMessage};

pub mod native;
pub use native::gl;

pub struct SenderCollection {
    map: HashMap<native::HWND, Sender<WindowMessage>>
}

impl SenderCollection
{
    pub fn new() -> Self
    {
        Self
        {
            map: HashMap::new()
        }
    }

    pub fn make_context(&self)
    {
        unsafe { native::make_context(self) };
    }

    pub fn register(&mut self, window: &Window, sender: Sender<WindowMessage>)
    {
        self.map.insert(window.handle, sender);
    }
}


impl Key
{
    fn from(key_code: native::WPARAM) -> Option<Self>
    {
        match key_code
        {
            0x08 => Some(Key::Backspace),
            0x09 => Some(Key::Tab),
            0x0D => Some(Key::Enter),
            0x10 => Some(Key::Shift),
            0x11 => Some(Key::Ctrl),
            0x14 => Some(Key::CapsLock),
            0x1B => Some(Key::Escape),
            0x20 => Some(Key::Space),
            0x21 => Some(Key::PgUp),
            0x22 => Some(Key::PgDown),
            0x23 => Some(Key::End),
            0x24 => Some(Key::Home),
            0x25 => Some(Key::LeftArrow),
            0x26 => Some(Key::UpArrow),
            0x27 => Some(Key::RightArrow),
            0x28 => Some(Key::DownArrow),
            0x2D => Some(Key::Insert),
            0x2E => Some(Key::Delete),
            0x30 => Some(Key::K0),
            0x31 => Some(Key::K1),
            0x32 => Some(Key::K2),
            0x33 => Some(Key::K3),
            0x34 => Some(Key::K4),
            0x35 => Some(Key::K5),
            0x36 => Some(Key::K6),
            0x37 => Some(Key::K7),
            0x38 => Some(Key::K8),
            0x39 => Some(Key::K9),
            0x41 => Some(Key::KA),
            0x42 => Some(Key::KB),
            0x43 => Some(Key::KC),
            0x44 => Some(Key::KD),
            0x45 => Some(Key::KE),
            0x46 => Some(Key::KF),
            0x47 => Some(Key::KG),
            0x48 => Some(Key::KH),
            0x49 => Some(Key::KI),
            0x4A => Some(Key::KJ),
            0x4B => Some(Key::KK),
            0x4C => Some(Key::KL),
            0x4D => Some(Key::KM),
            0x4E => Some(Key::KN),
            0x4F => Some(Key::KO),
            0x50 => Some(Key::KP),
            0x51 => Some(Key::KQ),
            0x52 => Some(Key::KR),
            0x53 => Some(Key::KS),
            0x54 => Some(Key::KT),
            0x55 => Some(Key::KU),
            0x56 => Some(Key::KV),
            0x57 => Some(Key::KW),
            0x58 => Some(Key::KX),
            0x59 => Some(Key::KY),
            0x5A => Some(Key::KZ),
            _ => {
                println!("Unknown keycode: {:x?}", key_code);
                None
            }
        }
    }
}

impl WindowMessage
{
    fn from(message: native::WindowMessage, wparam: native::WPARAM, lparam: native::LPARAM) -> Option<Self>
    {
        match message
        {
            native::WindowMessage::WM_KEYDOWN => Some(WindowMessage::Keydown { key: Key::from(wparam) }),
            native::WindowMessage::WM_KEYUP => Some(WindowMessage::Keyup { key: Key::from(wparam) }),
            native::WindowMessage::WM_MOUSEMOVE => Some(WindowMessage::MouseMove {
                x: unsafe { native::get_x_param(lparam) },
                y: unsafe { native::get_y_param(lparam) }
            }),
            native::WindowMessage::WM_MOUSEWHEEL => Some(WindowMessage::MouseWheelScroll {
                delta: unsafe { native::get_wheel_delta_wparam(wparam) }
            }),
            native::WindowMessage::WM_LBUTTONDOWN => Some(WindowMessage::MouseButtonDown { button: MouseButton::Left }),
            native::WindowMessage::WM_LBUTTONUP => Some(WindowMessage::MouseButtonUp { button: MouseButton::Left }),
            native::WindowMessage::WM_RBUTTONDOWN => Some(WindowMessage::MouseButtonDown { button: MouseButton::Right }),
            native::WindowMessage::WM_RBUTTONUP => Some(WindowMessage::MouseButtonUp { button: MouseButton::Right }),
            native::WindowMessage::WM_MBUTTONDOWN => Some(WindowMessage::MouseButtonDown { button: MouseButton::Middle }),
            native::WindowMessage::WM_MBUTTONUP => Some(WindowMessage::MouseButtonUp { button: MouseButton::Middle }),
            _ => None
        }
    }
}

pub struct DeviceContext<'a>
{
    handle: native::HDC,
    window: &'a Window
}

impl<'a> DeviceContext<'a>
{
    fn new(window: &'a Window) -> Self
    {
        Self
        {
            handle: unsafe { native::GetDC(window.handle) },
            window: window
        }
    }

    pub fn create_opengl_context(&self) -> OpenGLContext
    {
        OpenGLContext::new(self)
    }

    pub fn swap_buffers(&self)
    {
        unsafe { native::SwapBuffers(self.handle) };
    }
}

impl<'a> Drop for DeviceContext<'a>
{
    fn drop(&mut self)
    {
        unsafe { native::ReleaseDC(self.window.handle, self.handle) };
    }
}

pub struct OpenGLContext<'a, 'b>
{
    handle: native::HGLRC,
    device_context: &'a DeviceContext<'b>
}

impl<'a, 'b> OpenGLContext<'a, 'b>
{
    fn new(device_context: &'a DeviceContext<'b>) -> Self
    {
        let opengl_context = unsafe { native::create_opengl_context(device_context.handle) };

        Self
        {
            handle: opengl_context,
            device_context: device_context
        }
    }

    pub fn make_current(&self)
    {
        match unsafe { native::wglMakeCurrent(self.device_context.handle, self.handle) }
        {
            0 => { println!("Could not make device context current"); }
            _ => { println!("Made device context current"); }
        }
    }

    pub fn load_functions(&self)
    {
        gl::load_with(|s| unsafe { native::oglLoadProc(CString::new(s).unwrap().as_ptr()) } as *const _);
    }
}

pub struct Window
{
    handle: native::HWND
}

unsafe impl Sync for Window {}
unsafe impl Send for Window {}

impl Window
{
    pub fn new() -> Self
    {
        Self {
            handle: unsafe { native::create_window() }
        }
    }

    pub fn get_device_context(&self) -> DeviceContext
    {
        DeviceContext::new(self)
    }

    pub fn track_mouse(&self)
    {
        unsafe { native::set_track_mouse_event(self.handle) };
    }

    pub fn message_loop(&self)
    {
        unsafe { native::do_message_loop(self.handle) }
    }

    pub fn set_mouse_position(&self, x: i32, y: i32)
    {
        unsafe { native::set_mouse_position(self.handle, x, y) }
    }

    pub fn close(&self)
    {
        unsafe { native::close_window(self.handle) }
    }
}

impl<'a, 'b> std::ops::Drop for OpenGLContext<'a, 'b>
{
    fn drop(&mut self)
    {
        unsafe { native::wglDeleteContext(self.handle); };
    }
}

impl std::ops::Drop for Window
{
    fn drop(&mut self)
    {
        unsafe { native::DestroyWindow(self.handle); };
        println!("Destroying Window");
    }
}
