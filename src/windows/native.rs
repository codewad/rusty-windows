use libc::{c_uint, c_int};

pub mod gl
{
    include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
}

pub enum HWND__ {}
pub type HWND = *const HWND__;

pub enum HDC__ {}
pub type HDC = *const HDC__;

pub enum HGLRC__ {}
pub type HGLRC = *const HGLRC__;

#[allow(non_camel_case_types)]
pub enum function__ {}
pub type PROC = *mut function__;

type BOOL = c_int;
type LRESULT = isize;
pub type LPARAM = isize;
pub type WPARAM = isize;
pub type CHAR = libc::c_char;
pub type LPCSTR = *const CHAR;

#[allow(non_camel_case_types)]
#[derive(Debug, Clone, Copy)]
pub enum WindowMessage
{
    WM_CREATE,
    WM_DESTROY,
    WM_SIZE,
    WM_ACTIVATE,
    WM_CLOSE,
    WM_QUIT,
    WM_SHOWWINDOW,
    WM_SETCURSOR,
    WM_GETMINMAXINFO,
    WM_GETICON,
    WM_NCCREATE,
    WM_NCDESTROY,
    WM_NCHITTEST,
    WM_NCACTIVATE,
    WM_NCMOUSEMOVE,
    WM_KEYDOWN,
    WM_KEYUP,
    WM_CHAR,
    WM_MOUSEMOVE,
    WM_LBUTTONDOWN,
    WM_LBUTTONUP,
    WM_RBUTTONDOWN,
    WM_RBUTTONUP,
    WM_MBUTTONDOWN,
    WM_MBUTTONUP,
    WM_MOUSEWHEEL,
    WM_SIZING,
    WM_IME_NOTIFY,
    WM_NCMOUSELEAVE,
    WM_MOUSELEAVE,
    WM_DWMNCRENDERINGCHANGED,
}

impl WindowMessage
{
    fn from(value: c_uint) -> Option<WindowMessage>
    {
        match value
        {
            0x0001 => Some(WindowMessage::WM_CREATE),
            0x0002 => Some(WindowMessage::WM_DESTROY),
            0x0005 => Some(WindowMessage::WM_SIZE),
            0x0006 => Some(WindowMessage::WM_ACTIVATE),
            0x0010 => Some(WindowMessage::WM_CLOSE),
            0x0012 => Some(WindowMessage::WM_QUIT),
            0x0018 => Some(WindowMessage::WM_SHOWWINDOW),
            0x0020 => Some(WindowMessage::WM_SETCURSOR),
            0x0024 => Some(WindowMessage::WM_GETMINMAXINFO),
            0x007F => Some(WindowMessage::WM_GETICON),
            0x0081 => Some(WindowMessage::WM_NCCREATE),
            0x0082 => Some(WindowMessage::WM_NCDESTROY),
            0x0084 => Some(WindowMessage::WM_NCHITTEST),
            0x0086 => Some(WindowMessage::WM_NCACTIVATE),
            0x00A0 => Some(WindowMessage::WM_NCMOUSEMOVE),
            0x0100 => Some(WindowMessage::WM_KEYDOWN),
            0x0101 => Some(WindowMessage::WM_KEYUP),
            0x0102 => Some(WindowMessage::WM_CHAR),
            0x0200 => Some(WindowMessage::WM_MOUSEMOVE),
            0x0201 => Some(WindowMessage::WM_LBUTTONDOWN),
            0x0202 => Some(WindowMessage::WM_LBUTTONUP),
            0x0204 => Some(WindowMessage::WM_RBUTTONDOWN),
            0x0205 => Some(WindowMessage::WM_RBUTTONUP),
            0x0207 => Some(WindowMessage::WM_MBUTTONDOWN),
            0x0208 => Some(WindowMessage::WM_MBUTTONUP),
            0x020a => Some(WindowMessage::WM_MOUSEWHEEL),
            0x0214 => Some(WindowMessage::WM_SIZING),
            0x0282 => Some(WindowMessage::WM_IME_NOTIFY),
            0x02a2 => Some(WindowMessage::WM_NCMOUSELEAVE),
            0x02a3 => Some(WindowMessage::WM_MOUSELEAVE),
            0x031f => Some(WindowMessage::WM_DWMNCRENDERINGCHANGED),
            _ => None
        }
    }
}

fn get_handled_result(message: WindowMessage) -> isize
{
    match message
    {
        WindowMessage::WM_KEYDOWN   => 0,
        WindowMessage::WM_KEYUP     => 0,
        WindowMessage::WM_CHAR      => 0,
        WindowMessage::WM_MOUSEMOVE => 0,
        WindowMessage::WM_SIZING    => 1,
        _ => 0
    }
}

#[no_mangle]
fn window_proc(context: &mut super::SenderCollection, window_handle: HWND, message: c_uint, wparam: WPARAM, lparam: LPARAM, handled: *mut BOOL) -> LRESULT
{
    unsafe { *handled = 0; }
    match context.map.get(&window_handle)
    {
        Some(sender) => {
            match WindowMessage::from(message)
            {
                Some(WindowMessage::WM_NCDESTROY) => { context.map.remove(&window_handle); }
                Some(WindowMessage::WM_NCMOUSEMOVE) => {}
                Some(WindowMessage::WM_NCHITTEST) => {}
                Some(WindowMessage::WM_SETCURSOR) => {}
                Some(WindowMessage::WM_GETICON) => {}
                Some(WindowMessage::WM_CHAR) => {}
                Some(WindowMessage::WM_DWMNCRENDERINGCHANGED) => {},
                Some(window_message) =>
                {
                    match super::WindowMessage::from(window_message, wparam, lparam)
                    {
                        Some(message) => {
                            match sender.send(message)
                            {
                                Ok(_) =>
                                {
                                    unsafe { *handled = 1; }
                                    return get_handled_result(window_message);
                                }
                                _ => {}
                            }
                        }
                        None => println!("Unknown Translated Message: ({:x?}) {:?} (w 0x{:x?}) (l 0x{:x?})", window_handle, window_message, wparam, lparam)
                    }
                },
                None => println!("Unknown Native Message: ({:x?}) 0x{:0>4x} (w 0x{:x?}) (l 0x{:x?})", window_handle, message, wparam, lparam)
            }
        },
        None => println!("Unknown Window")
    }

    return 0;
}

#[link(name = "windows", kind = "static")]
extern
{
    #[allow(improper_ctypes)]
    pub fn make_context(context: &super::SenderCollection);

    pub fn create_window() -> HWND;
    pub fn DestroyWindow(handle: HWND);
    pub fn close_window(handle: HWND);

    pub fn GetDC(handle: HWND) -> HDC;
    pub fn ReleaseDC(handle: HWND, context: HDC);

    pub fn do_message_loop(handle: HWND);

    pub fn get_x_param(wparam: WPARAM) -> libc::c_int;
    pub fn get_y_param(wparam: WPARAM) -> libc::c_int;
    pub fn get_wheel_delta_wparam(wparam: WPARAM) -> libc::c_int;

    pub fn create_opengl_context(context: HDC) -> HGLRC;
    pub fn SwapBuffers(handle: HDC);
    pub fn wglMakeCurrent(device_context: HDC, opengl_context: HGLRC) -> BOOL;
    pub fn wglDeleteContext(context: HGLRC);
    pub fn oglLoadProc(name: LPCSTR) -> PROC;

    pub fn SetCursorPos(x: i32, y: i32);
    pub fn set_mouse_position(handle: HWND, x: i32, y: i32);
    pub fn set_track_mouse_event(handle: HWND) -> BOOL;
    pub fn GetLastError() -> i32;
}
