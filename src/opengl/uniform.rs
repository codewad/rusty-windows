use crate::windows::gl;
use rusty_math::ogl::{Mat3, Mat4, Vec3};

pub struct Uniform
{
    handle: gl::types::GLint
}

pub trait SetFrom<T, U>
where T: Into<U>
{
    fn set_from(&self, value: T);
}

impl SetFrom<gl::types::GLint, gl::types::GLint> for Uniform
{
    fn set_from(&self, value: gl::types::GLint)
    {
        unsafe { gl::Uniform1i(self.handle, value) };
    }
}

impl SetFrom<&Mat4<f32>, [gl::types::GLfloat; 16]> for Uniform
{
    fn set_from(&self, value: &Mat4<f32>)
    {
        let raw : [gl::types::GLfloat; 16] = value.into();
        unsafe { gl::UniformMatrix4fv(self.handle, 1, gl::FALSE, raw.as_ptr()) };
    }
}

impl SetFrom<&Mat3<f32>, [gl::types::GLfloat; 9]> for Uniform
{
    fn set_from(&self, value: &Mat3<f32>)
    {
        let raw : [gl::types::GLfloat; 9] = value.into();
        unsafe { gl::UniformMatrix3fv(self.handle, 1, gl::FALSE, raw.as_ptr()) };
    }
}

impl SetFrom<&Vec3<f32>, [gl::types::GLfloat; 3]> for Uniform
{
    fn set_from(&self, value: &Vec3<f32>)
    {
        let raw : [gl::types::GLfloat; 3] = value.into();
        unsafe { gl::Uniform3fv(self.handle, 1, raw.as_ptr()) };
    }
}

impl Uniform
{
    pub fn new(handle: gl::types::GLint) -> Self
    {
        Self
        {
            handle: handle
        }
    }
}
