extern crate rusty_math;

mod shaders;
mod uniform;
mod vectorbuffer;
mod context_state;

use crate::libc;
use crate::windows::gl;
use core::ffi;
use std::fmt;

pub use shaders::{ShaderProgram, VertexShader, FragmentShader};
pub use uniform::{Uniform, SetFrom};
pub use vectorbuffer::VectorBuffer;
pub use context_state::{ContextState, ArrayBuffer};

enum GLenumLocal
{
    Enum(gl::types::GLenum)
}

impl fmt::Display for GLenumLocal
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
    {
        match self
        {
            GLenumLocal::Enum(gl::DEBUG_SEVERITY_HIGH) => write!(f, "DEBUG_SEVERITY_HIGH"),
            GLenumLocal::Enum(gl::DEBUG_SEVERITY_LOW) => write!(f, "DEBUG_SEVERITY_LOW"),
            GLenumLocal::Enum(gl::DEBUG_SEVERITY_MEDIUM) => write!(f, "DEBUG_SEVERITY_MEDIUM"),
            GLenumLocal::Enum(gl::DEBUG_SEVERITY_NOTIFICATION) => write!(f, "DEBUG_SEVERITY_NOTIFICATION"),
            GLenumLocal::Enum(gl::DEBUG_SOURCE_API) => write!(f, "DEBUG_SOURCE_API"),
            GLenumLocal::Enum(gl::DEBUG_TYPE_OTHER) => write!(f, "DEBUG_TYPE_OTHER"),
            GLenumLocal::Enum(x) => write!(f, "{:X}", x)
        }
    }
}

extern "system" fn callback(
    source: gl::types::GLenum,
    gltype: gl::types::GLenum,
    id: gl::types::GLuint,
    severity: gl::types::GLenum,
    _length: gl::types::GLsizei,
    message: *const gl::types::GLchar,
    _: *mut gl::types::GLvoid,
) {
    unsafe {
        use std::ffi::CStr;
        let rust_message = CStr::from_ptr(message).to_str().unwrap().to_owned();
        println!("GL Debug Message:");
        println!("  source: {}", GLenumLocal::Enum(source));
        println!("  type: {}", GLenumLocal::Enum(gltype));
        println!("  id: {}", GLenumLocal::Enum(id));
        println!("  severity: {}", GLenumLocal::Enum(severity));
        println!("  Message: {:?}", rust_message);
    }
}

pub fn enable_debug() {
    unsafe { gl::Enable(gl::DEBUG_OUTPUT) };
    unsafe { gl::DebugMessageCallback(Some(callback), 0 as _) };
}

fn generate_buffer_handle() -> gl::types::GLuint
{
    let mut handle: gl::types::GLuint = 0;
    unsafe { gl::GenBuffers(1, &mut handle) };
    return handle;
}

pub struct VertexArrayObject
{
    handle: gl::types::GLuint
}

pub struct ElementArrayBuffer
{
    handle: gl::types::GLuint
}

pub struct Texture
{
    handle: gl::types::GLuint
}

pub trait Bind
{
    fn bind(&self);
}

pub trait Buffer
{
    fn buffer_data(&self, size: isize, data: *mut libc::c_void);
}


impl Drop for VertexArrayObject
{
    fn drop(&mut self)
    {
        unsafe { gl::DeleteVertexArrays(1, &self.handle) };
    }
}

impl Drop for Texture
{
    fn drop(&mut self)
    {
        unsafe { gl::DeleteVertexArrays(1, &self.handle) };
    }
}

impl VertexArrayObject
{
    pub fn new() -> Self
    {
        let mut vertex_array_id : gl::types::GLuint = 0;
        unsafe { gl::GenVertexArrays(1, &mut vertex_array_id) };

        Self
        {
            handle: vertex_array_id
        }
    }
}

impl Texture
{
    pub fn new() -> Self
    {
        let mut handle = gl::types::GLuint::default();
        unsafe { gl::GenTextures(1, &mut handle) };

        Self
        {
            handle: handle
        }
    }

    pub fn set(&self, width: i32, height: i32, buffer: *const ffi::c_void)
    {
        self.bind();
        unsafe { gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RGBA as i32,
                                width, height, 0, gl::RGBA, gl::UNSIGNED_BYTE,
                                buffer) };
    }

    pub fn set_params(&self)
    {
        unsafe { gl::TexParameterf(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::NEAREST as f32) };
        unsafe { gl::TexParameterf(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::NEAREST as f32) };
    }
}

impl Bind for VertexArrayObject
{
    fn bind(&self)
    {
        unsafe { gl::BindVertexArray(self.handle) };
    }
}

impl Bind for Texture
{
    fn bind(&self)
    {
        unsafe { gl::BindTexture(gl::TEXTURE_2D, self.handle) };
    }
}

impl ElementArrayBuffer
{
    pub fn new() -> Self
    {
        Self {
            handle: generate_buffer_handle()
        }
    }
}

impl Bind for ElementArrayBuffer
{
    fn bind(&self)
    {
        unsafe { gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, self.handle) };
    }
}

impl Buffer for ElementArrayBuffer
{
    fn buffer_data(&self, size: isize, data: *mut libc::c_void)
    {
        unsafe { gl::BufferData(gl::ELEMENT_ARRAY_BUFFER, size, data, gl::STATIC_DRAW) };
    }
}
