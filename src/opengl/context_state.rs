use crate::windows::{gl, OpenGLContext};
use super::{generate_buffer_handle, Bind, Buffer};

pub type ArrayBuffer = BufferHandle;

pub struct ContextState<'a, 'b>
{
    _context: OpenGLContext<'a, 'b>,
    bind_array_buffer_state: BindBufferState
}

impl<'a, 'b> From<OpenGLContext<'a, 'b>> for ContextState<'a, 'b>
{
    fn from(context: OpenGLContext<'a, 'b>) -> Self
    {
        Self
        {
            _context: context,
            bind_array_buffer_state: BindBufferState::new(gl::ARRAY_BUFFER)
        }
    }
}

impl<'a, 'b> ContextState<'a, 'b>
{
    pub fn create_array_buffer(&self) -> ArrayBuffer
    {
        ArrayBuffer::new(&self)
    }

    pub(super) fn generate_buffer_handle(&self) -> gl::types::GLuint
    {
        generate_buffer_handle()
    }

    pub(super) fn bind_array_buffer(&mut self, handle: gl::types::GLuint)
    {
        self.bind_array_buffer_state.bind_handle(handle);
    }
}

struct BindBufferState
{
    buffer_type: gl::types::GLenum,
    current: Option<gl::types::GLuint>
}

impl BindBufferState
{
    fn new(buffer_type: gl::types::GLenum) -> Self
    {
        Self
        {
            buffer_type: buffer_type,
            current: None
        }
    }

    fn bind_handle(&mut self, handle: gl::types::GLuint)
    {
        if let Some(h) = self.current
        {
            if h != handle
            {
                println!("bind {:?} {:?} previously {:?}", self.buffer_type, handle, h);
                unsafe { gl::BindBuffer(self.buffer_type, handle) };
                self.current = Some(handle);
            }
        }
        else
        {
            println!("bind {:?} {:?} previously null", self.buffer_type, handle);
            unsafe { gl::BindBuffer(self.buffer_type, handle) };
            self.current = Some(handle);
        }
    }
}

pub struct BufferHandle
{
    handle: gl::types::GLuint
}

impl BufferHandle
{
    pub fn new(context: &ContextState<'_,'_>) -> Self
    {
        Self {
            handle: context.generate_buffer_handle()
        }
    }

    pub fn bind(&self, context: &mut ContextState<'_,'_>)
    {
        context.bind_array_buffer(self.handle);
    }
}

impl Buffer for ArrayBuffer
{
    fn buffer_data(&self, size: isize, data: *mut libc::c_void)
    {
        println!("bufferdata array buffer {:?}", self.handle);
        unsafe { gl::BufferData(gl::ARRAY_BUFFER, size, data, gl::STATIC_DRAW) };
    }
}

impl Drop for BufferHandle
{
    fn drop(&mut self)
    {
        unsafe { gl::DeleteBuffers(1, &self.handle) };
    }
}
