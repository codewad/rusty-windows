use crate::opengl::{Bind, Buffer};

#[derive(Debug)]
pub struct VectorBuffer<'a, 'b, T, U>
{
    data: &'b Vec<T>,
    buffer: &'a U
}

impl<'a, 'b, T, U> VectorBuffer<'a, 'b, T, U>
{
    pub fn new(vector: &'b Vec<T>, buffer: &'a U) -> Self
    {
        Self
        {
            data: vector,
            buffer: buffer
        }
    }

    pub fn len(&self) -> usize
    {
        self.data.len()
    }

    pub fn data(&self) -> &Vec<T>
    {
        &self.data
    }
}

impl<'a, 'b, T, U> VectorBuffer<'a, 'b, T, U>
where U: Bind
{
    pub fn bind(&self)
    {
        self.buffer.bind();
    }
}

impl<'a, 'b, T, U> VectorBuffer<'a, 'b, T, U>
where U: Buffer
{
    pub fn set(&self)
    {
        self.buffer.buffer_data(std::mem::size_of::<T>() as isize * self.data.len() as isize, self.data.as_ptr() as *mut libc::c_void);
    }
}
