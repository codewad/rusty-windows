use crate::windows::gl;

use std::fmt;
use std::ffi::CString;
use super::Uniform;

#[derive(Debug, Clone)]
pub struct ShaderError
{
    message: String
}

impl fmt::Display for ShaderError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.message)
    }
}

fn get_error() -> gl::types::GLenum
{
    unsafe { gl::GetError() }
}

fn get_opengl_string<F>(length: isize, f: F) -> String
where F: Fn(isize, *mut u8)
{
    let mut buffer : Vec<u8> = Vec::with_capacity(length as usize + 1);
    unsafe { buffer.set_len(length as usize) };
    f(length, buffer.as_mut_ptr());

    String::from_utf8(buffer).unwrap()
}

fn compile_shader(shader_id: gl::types::GLuint, source: &CString) -> Result<(), ShaderError>
{
    let mut result : gl::types::GLint = 0;
    unsafe { gl::ShaderSource(shader_id, 1, &source.as_ptr() as *const *const i8, 0 as *const i32) };
    unsafe { gl::CompileShader(shader_id) };
    unsafe { gl::GetShaderiv(shader_id, gl::COMPILE_STATUS, &mut result) };
    if result == 0
    {
        let mut log_length : gl::types::GLint = 0;
        unsafe { gl::GetShaderiv(shader_id, gl::INFO_LOG_LENGTH, &mut log_length) };

        if log_length > 0
        {
            let message = get_opengl_string(log_length as isize, |length: isize, buffer: *mut u8| {
                unsafe { gl::GetShaderInfoLog(shader_id, length as i32, 0 as *mut i32, buffer as *mut i8) };
            });
            return Err(ShaderError { message: message });
        }
    }

    Ok(())
}

trait AsCPtr
{
    fn as_c_ptr(&self) -> *const i8;
}

impl AsCPtr for str
{
    fn as_c_ptr(&self) -> *const i8
    {
        CString::new(self).unwrap().as_ptr()
    }
}

pub struct ShaderProgram
{
    pub handle: gl::types::GLuint
}

impl ShaderProgram
{
    pub fn new() -> Self
    {
        Self {
            handle: unsafe { gl::CreateProgram() }
        }
    }

    fn check_link_status(&self) -> Result<(), String>
    {
        let result = self.get_program_iv(gl::LINK_STATUS).unwrap();
        if result != 0
        {
            return Ok(());
        }

        println!("Error compiling shader {:?} result: {:?}", self.handle, result);
        let log_length = self.get_program_iv(gl::INFO_LOG_LENGTH).unwrap();
        let mut message : String = String::default();

        if log_length > 0
        {
            message = get_opengl_string(log_length as isize, |length: isize, buffer: *mut u8| {
                unsafe { gl::GetProgramInfoLog(self.handle, length as i32, 0 as *mut i32, buffer as *mut i8) };
            });
        }

        return Err(message);
    }

    pub fn use_program(&self)
    {
        unsafe { gl::UseProgram(self.handle) };
    }

    pub fn bind_location(&self, location: gl::types::GLuint, name: &str)
    {
        // TODO: This still needs to handle cases when the uniform name doesn't exist
        unsafe { gl::BindAttribLocation(self.handle, location, name.as_c_ptr()) };
    }

    pub fn get_uniform(&self, name: &str) -> Result<Uniform, String>
    {
        let c_name = match CString::new(name)
        {
            Err(_e) => { return Err("Failed to convert uniform name to string".to_string()) },
            Ok(s) => s
        };

        let id = unsafe { gl::GetUniformLocation(self.handle, c_name.as_ptr()) };

        match id
        {
            -1 => Err(format!("Could not find uniform: {}", name)),
            _ => Ok(Uniform::new(id))
        }
    }

    fn get_program_iv(&self, name: gl::types::GLenum) -> Result<gl::types::GLint, gl::types::GLenum>
    {
        let mut value = gl::types::GLint::default();
        unsafe { gl::GetProgramiv(self.handle, name, &mut value) };
        let error = get_error();

        return match error
        {
            gl::NO_ERROR => Ok(value),
            _ => Err(error)
        };
    }

    pub fn get_all_uniforms(&self) -> Vec<String>
    {
        let mut result : Vec<String> = vec![];

        let uniforms = self.get_program_iv(gl::ACTIVE_UNIFORMS).unwrap();
        let max_length = self.get_program_iv(gl::ACTIVE_UNIFORM_MAX_LENGTH).unwrap();
        let mut written = gl::types::GLsizei::default();
        let mut size = gl::types::GLint::default();
        let mut uniform_type = gl::types::GLenum::default();

        for i in 0..uniforms as u32
        {
            let mut buffer : Vec<u8> = Vec::with_capacity(max_length as usize + 1);
            unsafe { buffer.set_len(max_length as usize) };
            unsafe { gl::GetActiveUniform(self.handle, i, max_length, &mut written, &mut size, &mut uniform_type, buffer.as_mut_ptr() as *mut i8) };
            let name = String::from_utf8(buffer[0..written as usize].to_vec()).unwrap();
            result.push(name);
        }

        return result;
    }

    pub fn link(&self) -> Result<(), String>
    {
        unsafe { gl::LinkProgram(self.handle) };
        self.check_link_status()
    }

    fn attach_shader(&self, shader_handle: gl::types::GLuint)
    {
        unsafe { gl::AttachShader(self.handle, shader_handle) };
    }

    pub fn attach_vertex_shader(&self, shader: &VertexShader)
    {
        self.attach_shader(shader.handle);
    }

    pub fn attach_fragment_shader(&self, shader: &FragmentShader)
    {
        self.attach_shader(shader.handle);
    }

    fn detach_shader(&self, shader_handle: gl::types::GLuint)
    {
        unsafe { gl::DetachShader(self.handle, shader_handle) };
    }

    pub fn detach_vertex_shader(&self, shader: &VertexShader)
    {
        self.detach_shader(shader.handle);
    }

    pub fn detach_fragment_shader(&self, shader: &FragmentShader)
    {
        self.detach_shader(shader.handle);
    }
}

impl Drop for ShaderProgram
{
    fn drop(&mut self)
    {
        unsafe { gl::DeleteProgram(self.handle) };
    }
}

pub struct VertexShader
{
    handle: gl::types::GLuint
}

impl VertexShader
{
    fn new() -> Self
    {
        Self {
            handle: unsafe { gl::CreateShader(gl::VERTEX_SHADER) }
        }
    }

    pub fn compile(source: &CString) -> Result<Self, ShaderError>
    {
        let shader = Self::new();
        match compile_shader(shader.handle, source)
        {
            Ok(()) => Ok(shader),
            Err(e) => Err(e)
        }
    }
}

impl Drop for VertexShader
{
    fn drop(&mut self)
    {
        unsafe { gl::DeleteShader(self.handle) };
    }
}

pub struct FragmentShader
{
    handle: gl::types::GLuint
}

impl FragmentShader
{
    fn new() -> Self
    {
        Self {
            handle: unsafe { gl::CreateShader(gl::FRAGMENT_SHADER) }
        }
    }

    pub fn compile(source: &CString) -> Result<Self, ShaderError>
    {
        let shader = Self::new();
        match compile_shader(shader.handle, source)
        {
            Ok(()) => Ok(shader),
            Err(e) => Err(e)
        }
    }
}

impl Drop for FragmentShader
{
    fn drop(&mut self)
    {
        unsafe { gl::DeleteShader(self.handle) };
    }
}
