SOURCE := $(wildcard src/*.c) $(wildcard src/*.rs) $(wildcard src/**/*.c) $(wildcard src/**/*.rs)

debug:
	cargo build --target=x86_64-pc-windows-gnu

release:
	cargo build --release --target=x86_64-pc-windows-gnu

check:
	cargo check --target=x86_64-pc-windows-gnu

clean:
	cargo clean
